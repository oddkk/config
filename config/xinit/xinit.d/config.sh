CONFIG_DIR=$(<~/.configdir)
XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config/}"

export MOZ_USE_XINPUT2=1
xset r rate 200 50 > /dev/null 2> /dev/null
setxkbmap no
xsetroot -cursor_name left_ptr
$CONFIG_DIR/bin/xrdb-update
xmodmap $XDG_CONFIG_HOME/xinit/xinit.d/xmodmap
