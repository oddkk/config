CONFIG_DIR=$(<~/.configdir)

function check_mpd_update() {
	while true; do
		#mpd idle player;
		twmnc -t "$(mpc current --wait)";
	done
}

xscreensaver -no-splash &
comptop --no-fading-openclose &
sxhkd &
twmnd &
emacs_hide_cursor.sh &
#check_mpd_update &
exec bspwm
