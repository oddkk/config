#/bin/bash

CONFIG_DIR=$(<~/.configdir)
XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config/}"

base_dir=$XDG_CONFIG_HOME/xinit/xinit.d/

tmpfile=$(mktemp "tmp.XXXXXXXXX.png")

fg="#010203"
bg="#18191A"

convert $base_dir/tile.xbm -fill $bg -opaque black -fill $fg -opaque white $tmpfile
feh --bg-tile $tmpfile
rm $tmpfile
