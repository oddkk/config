;;; .emacs -- Emacs config
;;; Commentary:
;;; My Emacs config
;;; Code:

;; Hide window elements
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)


(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
;;(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(setq use-package-always-ensure t)

(require 'use-package)
(require 'bind-key)
(require 'diminish)

(use-package northcode-theme)
;; (use-package klere-theme)

;; (use-package cyberpunk-theme)

;; Set fonts
;; (set-face-attribute 'default nil :font "DejaVuSansMono" :height 120)
;; (set-face-attribute 'default nil :font "NotoMono" :height 120)

(set-face-attribute 'default nil :font "Terminus-16")

(use-package evil
  :init
  (setq evil-want-C-d-scroll t)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-w-in-emacs-state t)

  :config (evil-mode t))

(use-package evil-surround
  :config (global-evil-surround-mode 1))
;; ;;(use-package evil-org)
(use-package evil-commentary
  :config (evil-commentary-mode))
(use-package key-chord
  :config
  (key-chord-mode t)
  (key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
  (key-chord-define evil-visual-state-map "jk" 'evil-normal-state)
  )

(use-package ag)

(use-package helm
  :config
  (helm-mode t)
  (progn
    (bind-keys
     :map helm-map
     ("C-j". helm-next-line)
     ("C-k". helm-previous-line)
     ("C-l". helm-execute-persistent-action))

    (bind-keys
     :map helm-find-files-map
     ("C-l". helm-execute-persistent-action)
     ("C-b". helm-find-files-up-one-level))))

(use-package projectile
  :config (projectile-global-mode t))

(use-package helm-projectile
  :config
  (setq projectile-completion-system 'helm)
  (helm-projectile-on)
  )

(use-package helm-ag)

(use-package helm-company)
(use-package helm-swoop)

(use-package magit)

; C/C++ disassembler
(use-package rmsbolt)

; Image manipulation
(use-package blimp)

;; (use-package evil-magit
;;   :config (evil-magit-init))

;; (use-package auto-complete
;;   :config
;;   (ac-config-default)
;;   (add-hook 'prog-mode-hook 'auto-complete-mode)
;;   )

;; (use-package auto-complete-c-headers
;;   :config
;;   (let ((acc-hook (lambda () (add-to-list 'ac-sources 'ac-source-c-headers))))
;;     (add-hook 'c++-mode-hook acc-hook)
;;     (add-hook 'c-mode-hook acc-hook)))


;; (use-package auto-complete-clang
;;   :config
;;   (let ((acc-hook (lambda () (add-to-list 'ac-sources 'ac-source-clang))))
;;     (add-hook 'c++-mode-hook acc-hook)
;;     (add-hook 'c-mode-hook acc-hook)))

(use-package haskell-mode
  :mode "\\.hs\\'")

(use-package rust-mode
  :mode "\\.rs\\'")

(use-package web-mode
  :mode ("\\.html\\'" "\\.jinja2\\'" "\\.tpl\\'"))

(use-package glsl-mode
  :mode ("\\.vert\\'" "\\.frag\\'" "\\.fsh\\'" "\\.vsh\\'"))

(use-package bison-mode
  :mode "\\.y\\'")

(use-package scss-mode
  :mode "\\.scss\\'")

(use-package php-mode
  :mode "\\.php\\'")

(use-package csharp-mode
  :mode "\\.cs\\'")

(use-package typescript-mode
  :mode ("\\.ts\\'" "\\.tsx\\'"))

(use-package emmet-mode
  :config
  (add-hook 'sgml-mode-hook 'emmet-mode)
  (add-hook 'web-mode-hook 'emmet-mode)
  )

(use-package nyan-mode)

(use-package ensime
  :ensure t
  :pin melpa-stable
  :config (setq ensime-startup-notification nil))

(use-package sbt-mode
  :ensure t
  :pin melpa-stable)

(use-package scala-mode
  :ensure t
  :pin melpa-stable)

(bind-keys
 ("C-c a" . org-agenda)
 ("C-c s" . ag)
 ("C-c x" . helm-M-x)
 ("C-c g" . magit-status)
 ("C-c e" . eshell)
 ("C-c k" . helm-show-kill-ring)
 ("C-x C-f" . helm-find-files)
 )

(define-key evil-normal-state-map (kbd "g d") 'helm-swoop)

(diminish 'helm-mode)
(diminish 'undo-tree-mode)
(diminish 'auto-revert-mode)
(diminish 'company-mode)
(diminish 'evil-commentary-mode)
(diminish 'projectile-mode)
(diminish 'abbrev-mode)


(define-coding-system-alias 'UTF-8 'utf-8)
(define-coding-system-alias 'utf8 'utf-8)
(defalias 'yes-or-no-p 'y-or-n-p)


(defun save-all () "Save all files."
       (interactive)
       (save-some-buffers t))
(add-hook 'focus-out-hook 'save-all)

;; Fix indentation
(setq-default tab-width 4)
(setq-default c-basic-offset 4)

(add-hook 'python-mode-hook
	  (lambda ()
	    (setq tab-width 4)
	    (setq python-indent-offset 4)
	    (setq python-indent 4)))

;; Move backup files to tmp dir
(setq backup-directory-alist
	  `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
	  `((".*" ,temporary-file-directory t)))

;; Enable column numbers
(setq column-number-mode t)

;; Enable highlight of trailing whitespace
(setq-default show-trailing-whitespace t)

;; Subwords
;; (add-hook 'prog-mode-hook 'subword-mode)

;; Set up mode line
(setq-default mode-line-format
			  '("    "
				(:eval (case evil-state
						 ('normal "N")
						 ('insert "I")
						 ('visual "V")
						 ('emacs  "E")))
				"    "
				(:propertize (:eval (if buffer-read-only " RO: "))
							 face font-lock-warning-face)
				(:propertize "%b"
							 face font-lock-keyword-face)
				(:propertize
				 (:eval (when (magit-get-current-branch)
						  (concat " [" (magit-get-current-branch) "]")))
				 face font-lock-string-face)
				" "
				(:eval (if (buffer-modified-p) "(!!)"))
				" %04l : %n "
				(:propertize "%m"
							 face font-lock-constant-face)
				" %e "
				(:eval (format-time-string "%H:%M" (current-time)))
				" %-"))


;; (defvar user-mailconf nil)
;; (setq user-mailconf
;; 	  '(:maildir "~/.mail/"
;; 				 :name "Odd Kristian Kvarmestøl"
;; 				 :signature "----\nOdd Kristian Kvarmestøl"
;; 				 :accounts
;; 				 ((:name "gmail okknor"
;; 						 :email "okknor@gmail.com"
;; 						 :smtp "smtp.gmail.com"
;; 						 :inbox "Inbox"
;; 						 :sent "[Gmail]/Sent Mail"
;; 						 :archive "[Gmail]/All Mail"
;; 						 :draft "drafts"
;; 						 :trash "[Gmail]/Bin"
;; 						 :match-func (lambda (msg)
;; 									   (when msg
;; 										 (mu4e-message-contact-field-matches
;; 										  msg :to "okknor@gmail.com"))))
;; 				  (:name "gmail odd.k.kvarmestol"
;; 						 :email "odd.k.kvarmestol@gmail.com"
;; 						 :smtp "smtp.gmail.com"
;; 						 :inbox "Inbox"
;; 						 :sent "[Gmail]/Sent Mail"
;; 						 :archive "[Gmail]/All Mail"
;; 						 :draft "drafts"
;; 						 :trash "[Gmail]/Bin"
;; 						 :match-func (lambda (msg)
;; 									   (when msg
;; 										 (mu4e-message-contact-field-matches
;; 										  msg :to "odd.k.kvarmestol@gmail.com"))))
;; 				  )
;; 				 ))
;; ;; load mu4e if its installed
;; (if (file-exists-p "/usr/share/emacs/site-lisp/mu4e")
;; 	(progn
;; 	  (message "loading mu4e config")
;; 	  (load-mu4e-conf)))



(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ada-broken-indent 2)
 '(ada-indent 4)
 '(ada-indent-record-rel-type 4)
 '(ada-indent-renames 4)
 '(ada-language-version (quote ada2005))
 '(ada-use-indent 4)
 '(ada-when-indent 4)
 '(ada-with-indent 4)
 '(custom-safe-themes
   (quote
	("d6922c974e8a78378eacb01414183ce32bc8dbf2de78aabcc6ad8172547cb074" "d8f76414f8f2dcb045a37eb155bfaa2e1d17b6573ed43fb1d18b936febc7bbc2" default)))
 '(org-agenda-files (quote ("~/journal/agenda/spring18.org")))
 '(package-selected-packages
   (quote
	(cuda-mode blimp rmsbolt ensime sbt-mode northcode-theme php-mode emmet-mode helm-spotify-plus haskell-mode znc xkcd writeroom-mode web-mode use-package typescript-mode tango-plus-theme spacegray-theme scss-mode racer org-journal org-bullets nyan-mode nord-theme mu4e-maildirs-extension mu4e-alert monokai-theme mingus mark-multiple less-css-mode key-chord ido-vertical-mode highlight-parentheses helm-swoop helm-projectile helm-mu helm-company helm-ag haste glsl-mode foggy-night-theme flycheck fireplace fill-column-indicator expand-region evil-surround evil-org evil-mu4e evil-magit evil-commentary disaster cyberpunk-theme csharp-mode context-coloring company-web company-jedi company-c-headers cmake-ide cmake-font-lock cider)))
 '(safe-local-variable-values
   (quote
	((eval setq flycheck-clang-include-path
		   (projectile-expand-paths
			(quote
			 ("./include/")))
		   flycheck-gcc-include-path
		   (projectile-expand-paths
			(quote
			 ("./include/"))))))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:height 113 :family "NotoMono")))))

(provide 'emacs)
;;; .emacs ends here
