fish_vi_mode

if status --is-login
	set PATH $PATH ~/.bin
end

function fish_greeting
  status --is-login
end

# cd into virtual env directory
function __check_virtualenv --on-variable PWD --description 'Activate or deactivate virtualenv'
	status --is-command-substitution; and return
	if test -d $PWD/env/;
		source $PWD/env/bin/activate.fish
	else
		type -q deactivate; and deactivate;
	end
end
