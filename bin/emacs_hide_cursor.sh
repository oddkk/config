#!/bin/bash

last_was_emacs=false

function get_window_class {
	xprop -id $(xdotool getwindowfocus) | awk '/WM_CLASS\(STRING\)/ {print $3}' | tr -d "\"," | cat
}

function get_cursor_id {
	xinput list | grep -Eo 'ouchpad\s*id\=[0-9]{1,2}' | grep -Eo '[0-9]{1,2}' | cat
}

function enable_cursor {
    xinput enable $(get_cursor_id)
}

function disable_cursor {
	xinput disable $(get_cursor_id)
}

function check {
	while read value; do
		class_name=$(get_window_class)
		if echo "$class_name" | grep --quiet -e "emacs"; then
			if [ "$last_was_emacs" = false ]; then
				echo "disable"
				disable_cursor
			fi;
			last_was_emacs=true
		else
			if [ "$last_was_emacs" = true ]; then
				echo "enable"
				enable_cursor
			fi;
			last_was_emacs=false
		fi;
	done <&0
}

#for title in $(xtitle -s); do
#	echo $title;
#	#check $title
#done
xtitle -s | check

