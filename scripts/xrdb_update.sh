#!/bin/sh

CONFIG_DIR=$(<~/.configdir)
XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config/}"

xrdb -override -I$HOME $XDG_CONFIG_HOME/xresources/Xresources

