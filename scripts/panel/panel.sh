#!/bin/bash

PANEL_FIFO="$XDG_RUNTIME_DIR/panelfifo"
#PANEL_FONT_FAMILY="-*-terminus-medium-r-*-*-12-*-*-*-*-*-iso10646-*"
PANEL_FONT_FAMILY="xft:SourceCodePro:size=8,xft:FontAwesome:size=8"
PANEL_HEIGHT=25
CONFIG_DIR=$(<~/.configdir)
SCRIPT_DIR=$CONFIG_DIR/scripts/panel/

if [ $(pgrep -cx panel.sh) -gt 1 ] ; then
	printf "%s\n" "The panel is already running, restarting." >&2
	pkill panel_bar.sh;
	sleep 0.5;
	#exit 1
fi

#trap 'trap - TERM; kill 0' INT TERM QUIT EXIT

[ -e "$PANEL_FIFO" ] && rm "$PANEL_FIFO"
mkfifo "$PANEL_FIFO"

bspc config top_padding $PANEL_HEIGHT

bspc control --subscribe > "$PANEL_FIFO" &

xtitle -sf 'T%s' > "$PANEL_FIFO" &

function clock {
date +S%H:%M:%S
}

function current_playing {
	echo -n "M/v:"
		for i in $(seq 1 $(echo $(amixer sget Master | awk -F"[][]" '/dB/ {print$2}' | head -c 2) / 10 | bc)); do echo -n "-"; done
	echo -n "/ "
	mpc current -f "%title% / %album% / %artist%"
	echo ""
}

function batteryinfo {
	bat=$(acpi -b | awk '{print$4;}')
	echo 'Z'"$bat"
}

function loadavg {
	echo 'L'$( cat /proc/loadavg )
}

function printinfo {
	while true
	do
		batteryinfo
		loadavg
		#current_playing
		clock
		sleep 1
	done
}

printinfo > "$PANEL_FIFO" &

#$SCRIPT_DIR/colors.sh
. ~/.bin/colr

cat "$PANEL_FIFO" | $SCRIPT_DIR/panel_bar.sh | lemonbar -g x$PANEL_HEIGHT++ -f "$PANEL_FONT_FAMILY" -f "xft:FontAwesome:size=8" -F $fg -B $bg &

