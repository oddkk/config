#export EDITOR=vim
export EDITOR='emacsclient -c -a "vim"'

alias ec=$EDITOR
alias em='emacsclient -nw -a "vim"'
alias mg='emacsclient -nw -e "(magit-status)"'

alias makeinstall="make && sudo make install"

CONFIG_DIR=$(<~/.configdir)

PATH="$PATH:$CONFIG_DIR/bin/"
