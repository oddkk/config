function env_cd(){
	cd $@;
	if stat env/ > /dev/null 2> /dev/null; then
		source env/bin/activate > /dev/null
	else
		deactivate 2> /dev/null
	fi
}

alias cd='env_cd'
