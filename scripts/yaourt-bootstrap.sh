#!/bin/bash

mkdir /tmp/package-query/
wget "https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=package-query" -O /tmp/package-query/PKGBUILD
cd /tmp/package-query/
makepkg -i

mkdir /tmp/yaourt/
wget "https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=yaourt" -O /tmp/yaourt/PKGBUILD
cd /tmp/yaourt/
makepkg -i


