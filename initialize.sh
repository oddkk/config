#!/bin/bash

BASEDIR=$(readlink -f $(dirname $0))
CONFIGDIR=$BASEDIR/config
XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config/}"

for dir in $(ls -d $CONFIGDIR/*); do
	new_dir="$XDG_CONFIG_HOME$(basename $dir)"
	[ -e $new_dir ] && rm -rf "$new_dir"
	ln -sf "$dir" "$new_dir"
	[ -x $new_dir/init.sh ] && . $new_dir/init.sh;
done;

bindir=$BASEDIR/bin
new_bindir=~/.bin
[ -e $new_bindir ] && rm -rf "$new_bindir"
ln -sf "$bindir" "$new_bindir"

# Set up config file for xresource
touch ~/.config/Xresources.config
echo $BASEDIR > ~/.configdir
